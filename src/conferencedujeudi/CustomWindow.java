/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conferencedujeudi;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 *
 * @author pierr
 */
class CustomWindow extends JFrame implements ActionListener{
private JMenuBar menu_bar1 = new JMenuBar();
    /* différents menus */
    private JMenu menu1 = new JMenu("Fichier");
    private JMenu menu2 = new JMenu("Conférence");
    private JMenuItem ItemFichier;
    private JMenuItem ItemConference;
    private JMenuItem ItemConference1;
    
public CustomWindow(String _title){
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.setVisible(true);//la rendre visible
    this.setSize(1200,900);//Définit sa taille : 1200 pixels de large et 900 pixels de haut
    this.setTitle(_title);//Définit un titre pour notre fenêtre
    this.menu_bar1.add(menu1);
    this.menu_bar1.add(menu2);
    this.setJMenuBar(menu_bar1);
    ItemFichier = new JMenuItem("Fermer");
    ItemConference = new JMenuItem("Voir la liste des conférences");
    ItemConference1 = new JMenuItem("Ajouter une conférence");
    menu1.add(ItemFichier);
    menu2.add(ItemConference);
    menu2.add(ItemConference1);
    ItemFichier.addActionListener(this) ;
    ItemConference.addActionListener(this) ;
    ItemConference1.addActionListener(this) ;
    
    Home Accueil = new Home();
    this.setContentPane(Accueil);
    this.setVisible(true);
    
    this.repaint();
    this.revalidate();
} 
public void actionPerformed(ActionEvent e){
if(e.getSource() == ItemFichier){
    System.exit(0);
}
}  
}

