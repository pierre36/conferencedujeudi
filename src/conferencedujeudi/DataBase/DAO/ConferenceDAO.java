/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conferencedujeudi.DataBase.DAO;

import com.mysql.jdbc.Connection;
import conferencedujeudi.DataBase.DatabaseUtilities;
import static conferencedujeudi.DataBase.DatabaseUtilities.getConnexion;
import conferencedujeudi.DataBase.POJO.Conference;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pierr
 */
public class ConferenceDAO {

    public ArrayList<Conference> getAll() {
        ArrayList<Conference> liste = new ArrayList<>();
        try {
            Connection cn = getConnexion();
            String query = "SELECT * FROM conference";
            ResultSet rs = null;
            Statement stmt = null;

            stmt = cn.createStatement();
            rs = stmt.executeQuery(query);

            while (rs.next()) {
                int idConference = rs.getInt("idConference");
                String titreConference = rs.getString("titreConference");
                Date dateConference = rs.getDate("dateConference");

                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(dateConference.getTime());
                int idConferencier = rs.getInt("idConferencier");
                int idSalle = rs.getInt("idSalle");
                int idTheme = rs.getInt("idTheme");
                Conference conference1 = new Conference(idConference, titreConference, idConferencier);
                liste.add(conference1);

            }
        } catch (SQLException ex) {
            Logger.getLogger(ConferenceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return liste;

    }

    /* public static ArrayList(conf)getAll();*/
    public static Conference get(int id) {
        Conference conference1 = null;
        try {
            PreparedStatement ps = null;
            ResultSet rs = null;
            Connection cn = getConnexion();
            String query = "SELECT * FROM Conference WHERE idConference = ?";

            ps = cn.prepareStatement(query);
            ps.setInt(1, id);
            rs = ps.executeQuery();

            while (rs.next()) {
                int idConference = rs.getInt("idConference");
                String titreConference = rs.getString("titreConference");
                Date dateConference = rs.getDate("dateConference");

                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(dateConference.getTime());
                int idConferencier = rs.getInt("idConferencier");
                int idSalle = rs.getInt("idSalle");
                int idTheme = rs.getInt("idTheme");
                conference1 = new Conference(idConference, titreConference, idConferencier);

            }

        } catch (SQLException ex) {
            Logger.getLogger(ConferenceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return conference1;
    }

    public static void delete(int id) {
        try {
            Connection cn = getConnexion();
            PreparedStatement ps = null;
            String query = "DELETE FROM Conference WHERE idConference = ?";
            ps = cn.prepareStatement(query);
            ps.setInt(1, id);
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(ConferenceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void update(Conference c) {
        Connection con = getConnexion();
        PreparedStatement ps = null;
        String query = "UPDATE conference SET titreConference=?,dateConference=?,idConferencier=?,idSalle=?,idTheme=? WHERE idConference=?";
        try {
            ps = con.prepareStatement(query);
            ps.setString(1, c.getTitreConference());
            ps.setDate(2, c.getsqlDate());
            ps.setInt(3, c.getIdConferencier());
            ps.setInt(4, c.getIdSalle());
            ps.setInt(5, c.getIdTheme());
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(ConferenceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void insert(Conference c) {
        Connection cn = getConnexion();
        PreparedStatement ps = null;
        String query = "INSERT INTO Conference('titreConference','dateConference','idConferencier','idSalle','idTheme')VALUES(?,?,?,?,?)";

        try {
            ps = cn.prepareStatement(query);
            ps.setString(1, c.getTitreConference());
            ps.setDate(2, c.getsqlDate());
            ps.setInt(3, c.getIdConferencier());
            ps.setInt(4, c.getIdSalle());
            ps.setInt(5, c.getIdTheme());
            ps.execute();

        } catch (SQLException e) {
            Logger.getLogger(ConferenceDAO.class.getName()).log(Level.SEVERE, null, e);

        }
    }
}
