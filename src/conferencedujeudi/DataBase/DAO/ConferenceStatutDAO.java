/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conferencedujeudi.DataBase.DAO;

import com.mysql.jdbc.Connection;
import com.sun.istack.internal.logging.Logger;
import conferencedujeudi.DataBase.DatabaseUtilities;
import conferencedujeudi.DataBase.POJO.Conference;
import conferencedujeudi.models.ConferenceStatut;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

/**
 *
 * @author pierr
 */
public class ConferenceStatutDAO {
public static ConferenceStatut get(int id) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection cn = DatabaseUtilities.getConnexion();
        String query = "SELECT * FROM ConferenceStatut WHERE idConference = ?";

        try {
            ps = cn.prepareStatement(query);
            ps.setInt(1, id);
            if (ps.execute()) {
                rs = ps.getResultSet();

            }
            while (rs.next()) {
                int idConference = rs.getInt("idConference");
                int idStatut = rs.getInt("idStatut");
                Date dateStatutConference = rs.getInt("dateStatutConference");

            }

        } catch (SQLException ex) {
            Logger.getLogger(ConferenceStatutDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}    
