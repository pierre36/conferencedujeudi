/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conferencedujeudi.DataBase.DAO;

import com.mysql.jdbc.Connection;
import com.sun.istack.internal.logging.Logger;
import conferencedujeudi.DataBase.DatabaseUtilities;
import conferencedujeudi.DataBase.POJO.Conference;
import conferencedujeudi.models.Conferencier;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

/**
 *
 * @author pierr
 */
public class ConferencierDAO {
    public static Conferencier get(int id) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection cn = DatabaseUtilities.getConnexion();
        String query = "SELECT * FROM Conferencier WHERE idConferencier = ?";

        try {
            ps = cn.prepareStatement(query);
            ps.setInt(1, id);
            if (ps.execute()) {
                rs = ps.getResultSet();

            }
            while (rs.next()) {
                int idConferencier = rs.getInt("idConferencier");
                String nomPrenomConferencier = rs.getString("nomPreomConferencier");
                int conferencierInterne = rs.getInt("conferencierInterne");
                int idConference = rs.getInt("idConference");
                String blocNoteConferencier = rs.getString("blocNoteConferencier");

            }

        } catch (SQLException ex) {
            Logger.getLogger(ConferencierDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
    

