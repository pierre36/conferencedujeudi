/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conferencedujeudi.DataBase.DAO;

import com.mysql.jdbc.Connection;
import com.sun.istack.internal.logging.Logger;
import conferencedujeudi.DataBase.DatabaseUtilities;
import conferencedujeudi.models.Inscription;
import conferencedujeudi.models.InscriptionStatut;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

/**
 *
 * @author pierr
 */
public class InscriptionStatutDAO {
public static InscriptionStatut get(int id) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection cn = DatabaseUtilities.getConnexion();
        String query = "SELECT * FROM InscriptionStatut WHERE idInscription = ?";

        try {
            ps = cn.prepareStatement(query);
            ps.setInt(1, id);
            if (ps.execute()) {
                rs = ps.getResultSet();

            }
            while (rs.next()) {
                int idInscription = rs.getInt("idInscription");
                int idStatut = rs.getInt("idStatut");
                Date dateStatutInscription = rs.getDate("dateStatutInscription");

            }

        } catch (SQLException ex) {
            Logger.getLogger(InscriptionStatutDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
