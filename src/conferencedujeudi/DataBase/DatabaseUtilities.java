/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conferencedujeudi.DataBase;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

/**
 *
 * @author pierr
 */
public class DatabaseUtilities {
    public static Connection getConnexion(){
    try{
        Class.forName("com.mysql.jdbc.Driver");
    }catch (ClassNotFoundException e){
        e.printStackTrace();
        return null;
    }
    Connection connection = null;
    
    try{
        connection = (Connection) DriverManager
                .getConnection("jdbc:mysql://localhost:3306/conference_du_jeudi", "root", "");
    } catch (SQLException e){
        e.printStackTrace();
        return null;
    }
    return connection;
}
    public static ResultSet exec(String query) throws SQLException{
        ResultSet resultSet = null;
        Statement stmt = null;
        Connection _c = getConnexion();
        stmt = _c.createStatement();
        if(stmt.execute(query)){
        resultSet = stmt.getResultSet();
    }
        
  return resultSet;           
}
}    
     
     
    