/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conferencedujeudi.DataBase.POJO;

import com.mysql.jdbc.Connection;
import static conferencedujeudi.DataBase.DatabaseUtilities.getConnexion;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author pierr
 */
public class Conference {
    private int idConference;
    private String titreConference;
    private Calendar dateConference;
    private int idConferencier;
    private int idSalle;
    private int idTheme;
    private String nomConferencier;
    private java.sql.Date sqlDate;
    


public Conference(){
}
public Conference(int _idConference,String _titreConference, int _idConferencier){
this.idConference = _idConference;
this.titreConference = _titreConference;
this.idConferencier = _idConferencier;


}
public Conference(int _idConference,String _titreConference,Calendar _dateConference, int _idConferencier,int _idSalle,int _idTheme,String _nomConferencier){
this.idConference = _idConference;
this.titreConference = _titreConference;
this.dateConference = _dateConference;
this.idConferencier = _idConferencier;
this.idSalle = _idSalle;
this.idTheme = _idTheme;
this.nomConferencier = _nomConferencier;

}
public int getIdConference() {
        return this.idConference;
    }

    public void setIdConference(int _idConference) {
        this.idConference = _idConference;
    }
    public String getTitreConference() {
        return this.titreConference;
    }

    public void setTitreConference(String _titreConference) {
        this.titreConference = _titreConference;
    }
    public Calendar getDateConference() {
        return this.dateConference;
    }

    public void setDateConference(Calendar _dateConference) {
        this.dateConference = _dateConference;
    }
    public int getIdConferencier() {
        return this.idConferencier;
    }
    public void setIdConferencier(int _idConferencier) {
        this.idConferencier = _idConferencier;
    }
    public int getIdSalle() {
        return this.idSalle;
    }
    public void setIdSalle(int _idSalle) {
        this.idSalle = _idSalle;
    }
   public int getIdTheme() {
        return this.idTheme;
    }
    public void setIdTheme(int _idTheme) {
        this.idTheme = _idTheme;
    } 
public String getNomConferencier() {
        return this.nomConferencier;
    }
    public void setNomConferencier(String _nomConferencier) {
        this.nomConferencier = _nomConferencier;
    }
    public String getDateString(){
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    return sdf.format(dateConference.getTime());
    }
    
    public java.sql.Date getsqlDate(){
        sqlDate = new java.sql.Date(this.dateConference.getTimeInMillis());
        return this.sqlDate;
    }
    
    public void Insert(){
        PreparedStatement ps = null;
        Connection _c = getConnexion();
        String query = " INSERT INTO conference (titreConference,dateConference,idConferencier,idSalle,idTheme) VALUES (?,?,?,?,?)"; 
        try{
            ps = _c.prepareStatement(query);
            ps.setString(1,this.getTitreConference());
            ps.setDate(2, this.getsqlDate());
            ps.setInt(3, this.getIdConferencier());
            
            ps.setInt(4,this.getIdSalle());
            ps.setInt(5,this.getIdTheme());
            ps.execute();
        }catch(SQLException ex){    
            System.err.println(ex.getMessage());
        }
    }
}