/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conferencedujeudi;

import conferencedujeudi.DataBase.POJO.Conference;
import conferencedujeudi.models.ConferenceModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author pierr
 */
public class Home extends JPanel {
    
    private int nbClics = 0;
    private JButton bouton;
    private JLabel lab2;
    private Conference conf1;
    private Conference conf2;
    private Conference conf3;
    private Calendar cal = Calendar.getInstance();
    private ConferenceModel modelConf;
    private JTable table;
    private JScrollPane scroll;
    
    public Home(){
        super();
        JLabel label = new JLabel("Bienvenue sur l'application");
        lab2 = new JLabel("");
        this.add(label);
        this.add(lab2);
         conf1 = new Conference(1,"Conférence 1",cal,3,109,2,"Gilbert");
         conf2 = new Conference(2,"Conférence 2",cal,5,209,8,"Gilberta");
         conf3 = new Conference(3,"Conférence 3",cal,7,309,3,"Gilberto");
         //Les variables commencent toujours par une MINUSCULE !!!!
         ArrayList<Conference>listConf = new ArrayList<>();
        listConf.add(conf1);
        listConf.add(conf2);
        listConf.add(conf3);
        modelConf = new ConferenceModel(listConf);
        table = new JTable(modelConf);
        table.setAutoCreateRowSorter(true);
        scroll = new JScrollPane(table);
        this.add(scroll);
        bouton = new JButton("CLIC &... surprise");
        bouton.addActionListener(new ActionListener(){
           
              
            @Override
            public void actionPerformed(ActionEvent e) {
                nbClics++;
                lab2.setText("Vous avez cliqué " + nbClics + " fois sur le bouton.");
            }
        });
        this.add(bouton);
    }
}