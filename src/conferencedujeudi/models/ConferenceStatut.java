/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conferencedujeudi.models;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author pierr
 */
public class ConferenceStatut {
    private int idConference;
    private Calendar dateStatutConference;
    private int idStatut;
    



public ConferenceStatut(int _idConference,Calendar _dateStatutConference, int _idStatut){
this.idConference = _idConference;
this.dateStatutConference = _dateStatutConference;
this.idStatut = _idStatut;


}

public int getIdConference() {
        return this.idConference;
    }

    public void setIdConference(int _idConference) {
        this.idConference = _idConference;
    }
    public Calendar getdateStatutConference() {
        return this.dateStatutConference;
    }

    public void setdateStatutConference(Calendar _dateStatutConference) {
        this.dateStatutConference = _dateStatutConference;
    }
    public int getidStatut() {
        return this.idStatut;
    }

    public void setidStatut(int _idStatut) {
        this.idStatut = _idStatut;
    } 
}
