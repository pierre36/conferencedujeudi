/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conferencedujeudi.models;

import com.mysql.jdbc.Connection;
import static conferencedujeudi.DataBase.DatabaseUtilities.getConnexion;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author pierr
 */
public class Conferencier{
    private int idConferencier;
    private String nomPrenomConferencier;
    private boolean conferencierInterne;
    private int idConference;
    private String blocNoteConferencier;
    



public Conferencier(int _idConferencier,String _nomPrenomConferencier, int _idConference){
this.idConferencier = _idConferencier;
this.nomPrenomConferencier = _nomPrenomConferencier;
this.idConference = _idConference;


}
public Conferencier(int _idConferencier,String _nomPrenomConferencier, int _idConference, boolean _conferencierInterne,String _blocNoteConferencier){
this.idConferencier = _idConferencier;
this.nomPrenomConferencier = _nomPrenomConferencier;
this.idConference = _idConference;
this.conferencierInterne = _conferencierInterne;
this.blocNoteConferencier = _blocNoteConferencier;


}

public int getIdConferencier() {
        return this.idConferencier;
    }

    public void setIdConferencier(int _idConferencier) {
        this.idConferencier = _idConferencier;
    }
    public String getnomPrenomConferencier() {
        return this.nomPrenomConferencier;
    }

    public void setnomPrenomConferencier(String _nomPrenomConferencier) {
        this.nomPrenomConferencier = _nomPrenomConferencier;
    }
    public int getIdConference() {
        return this.idConference;
    }

    public void setIdConference(int _idConference) {
        this.idConference = _idConference;
    }
    public boolean getconferencierInterne() {
        return this.conferencierInterne;
    }
    public void setconferencierInterne(boolean _conferencierInterne) {
        this.conferencierInterne = _conferencierInterne;
    }
    public String getblocNoteConferencier() {
        return this.blocNoteConferencier;
    }
    public void setblocNoteConferencier(String _blocNoteConferencier) {
        this.blocNoteConferencier = _blocNoteConferencier;
    }
   
}
    

