/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conferencedujeudi.models;

import java.util.Calendar;

/**
 *
 * @author pierr
 */
public class InscriptionStatut {
    private int idInscription;
    private int idStatut;
    private Calendar dateStatutInscription;
    



public InscriptionStatut(int _idInscription,int _idStatut, Calendar _dateStatutInscription){
this.idInscription = _idInscription;
this.idStatut = _idStatut;
this.dateStatutInscription = _dateStatutInscription;


}

public int getidInscription() {
        return this.idInscription;
    }

    public void setidInscription(int _idInscription) {
        this.idInscription = _idInscription;
    }
    public int getidStatut() {
        return this.idStatut;
    }

    public void setidStatut(int _idStatut) {
        this.idStatut = _idStatut;
    }
    public Calendar getdateStatutInscription() {
        return this.dateStatutInscription;
    }

    public void setdateStatutInscription(Calendar _dateStatutInscription) {
        this.dateStatutInscription = _dateStatutInscription;
    } 
}
  