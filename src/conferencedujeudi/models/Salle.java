/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conferencedujeudi.models;

/**
 *
 * @author pierr
 */
public class Salle {
    private int idSalle;
    private String nomSalle;
    private int nbPlaceSalle;
    
public Salle(int _idSalle,String _nomSalle,int _nbPlaceSalle){
this.idSalle = _idSalle;
this.nomSalle = _nomSalle;
this.nbPlaceSalle = _nbPlaceSalle;
}

public int getidSalle() {
        return this.idSalle;
    }

    public void idSalle(int _idSalle) {
        this.idSalle = _idSalle;
    }
    public String getnomSalle() {
        return this.nomSalle;
    }

    public void setnomSalle(String _nomSalle) {
        this.nomSalle = _nomSalle;
    }
    public int getnbPlaceSalle() {
        return this.nbPlaceSalle;
    }

    public void setnbPlaceSalle(int _nbPlaceSalle) {
        this.nbPlaceSalle = _nbPlaceSalle;
    }
}
